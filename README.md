drmtest
=======

Raison d'être
-------------

This repository contains programs and source code useful for understanding how
the graphics stack works from the userland's perspective on Linux and various
other related platforms using KMS (Kernel ModeSetting) and DRM (Direct Render
Management) for graphical output.

Tools
-----

* connector_test: shows any information exposed by libdrm to the user about
their graphics cards' connectors (connector type, monitor details, etc)

Building
--------

For a simple build, create a directory `build` onto the project directory's
root:

    $ mkdir build

Then, use meson:

    $ meson build

Finally, switch to `build`-directory and build the project using ninja:


    $ cd build/

    $ ninja

License
-------

This project is under GPLv3 or later at the user's choice. See more
on `LICENSE` or at
[the GPLv3 page by FSF](https://www.gnu.org/licenses/gpl-3.0.html).
