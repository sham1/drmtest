#include <xf86drm.h>
#include <xf86drmMode.h>

#include <stdlib.h>

#include <unistd.h>
#include <fcntl.h>

#include <stdbool.h>

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

const char *const help_text =
  "Usage:\n"
  "\t%1$s [dev_node]\n"
  "\t%1$s [--help]\n"
  "\nOptions:\n"
  "\tdev_node: The device node this tool will probe. Default: /dev/dri/card0\n"
  "\t--help: Show this message\n";

const char *connection_type[3] =
  {
    "Connected",
    "Disconnected",
    "Unknown",
  };

const char *connector_type[] =
  {
    "Unknown",
    "VGA",
    "DVI-I",
    "DVI-D",
    "DVI-A",
    "Composite",
    "S-VIDEO",
    "LVDS",
    "Component",
    "9PinDIN",
    "DisplayPort",
    "HDMI-A",
    "HDMI-B",
    "TV",
    "eDP",
    "VIRTUAL",
    "DSI",
    "DPI",
  };

const char *prop_type[] =
  {
    "Range",
    "Enum",
    "Blob",
    "Bitmask",
    "Range, Immutable",
    "Enum, Immutable",
    "Blob, Immutable",
    "Bitmask, Immutable",
  };

void parse_and_print_edid(const uint8_t *bytes);
const char *get_prop_type(uint32_t prop_flags);

int main (int argc, char **argv)
{
  int ret = EXIT_SUCCESS;
  if (argc > 2) {
    fprintf(stderr, help_text, argv[0]);
    return EXIT_FAILURE;
  }

  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "--help") == 0) {
      printf(help_text, argv[0]);
      return EXIT_SUCCESS;
    }
  }

  const char *const devname = argc == 2 ? argv[1] : "/dev/dri/card0";

  int cardFD = open(devname, O_RDONLY);

  drmModeResPtr resources = drmModeGetResources(cardFD);
  if (resources == NULL) {
    fputs("Couldn't acquire DRM resources!", stderr);
    ret = EXIT_FAILURE;
    goto err;
  }

  printf("Connectors for device: %s\n", devname);
  drmModeConnectorPtr connector = NULL;
  int i;
  for (i = 0, connector = drmModeGetConnector(cardFD, resources->connectors[i]);
       i < resources->count_connectors;
       ++i, connector = drmModeGetConnector(cardFD, resources->connectors[i])) {
    printf("Connector #%d (id: %"PRIu32")\n", i, resources->connectors[i]);

    // For some reason, connection type is 1-indexed...
    printf("\tConnection status: %s\n", connection_type[connector->connection - 1]);

    // Thankfully the connector type isn't that stupid.
    printf("\tConnector type: %s\n", connector_type[connector->connector_type]);

    if (connector->connection != DRM_MODE_CONNECTED)
      continue; // Let's not gather any more details from disconnected or unknown connection type

    printf("\tPhysical dimensions (in mm): %"PRIu32"x%"PRIu32"\n",
	   connector->mmWidth, connector->mmHeight);

    printf("\tMode amount: %d\n", connector->count_modes);

    drmModeModeInfo mode;
    int j;
    for (j = 0, mode = connector->modes[j];
	 j < connector->count_modes;
	 ++j, mode = connector->modes[j]) {
      printf("\t\tMode number: %d\n"
	     "\t\t\tName: %s\n"
	     "\t\t\tResolution: %"PRIu16"x%"PRIu16"\n"
	     "\t\t\tVblank frequency (in Hz): %"PRIu32"\n",
	     j, mode.name,
	     mode.hdisplay, mode.vdisplay,
	     mode.vrefresh);
      printf(mode.type & DRM_MODE_TYPE_PREFERRED ?
	     "\t\t\tThis mode is preferred\n" : "");
    }

    printf("\tProps and the EDID\n"
	   "\tProp amount: %d\n", connector->count_props);

    drmModePropertyPtr prop = NULL;
    for (j = 0, prop = drmModeGetProperty(cardFD, connector->props[j]);
	 j < connector->count_props;
	 ++j, prop = drmModeGetProperty(cardFD, connector->props[j])) {
      printf("\t\tProperty number: %d\n", j);
      printf("\t\t\tName: %s\n", prop->name);
      printf("\t\t\tType: %s\n", get_prop_type(prop->flags));
      // Now to put out specific types of output depending on the property type
      // And if it happens to be EDID
      if (prop->flags & DRM_MODE_PROP_BLOB) {
	puts("\t\t\tBlob value in hexadecimal:");
	drmModePropertyBlobPtr blobPtr =
	  drmModeGetPropertyBlob(cardFD, connector->prop_values[j]);
	uint8_t *bytes = blobPtr->data;
	for (size_t k = 0; k < blobPtr->length; ++k) {
	  printf("%02X", bytes[k] & 0xFF);
	}
	putchar('\n');

	// Now to check if this is EDID. It most likely is, but to not to risk it...
	if (strncmp(prop->name, "EDID", 4) == 0) {
	  parse_and_print_edid(blobPtr->data);
	}

	continue;
      }

      int k;

      if (prop->flags & DRM_MODE_PROP_ENUM) {
	printf("\t\t\tAmount of enum values: %d\n", prop->count_enums);
	puts("\t\t\tPossible enum values:");
	const char *sep = "\t\t\t\t";
	for (k = 0; k < prop->count_enums; ++k) {
	  printf("%s%s", sep, prop->enums[k].name);
	  sep = ", ";
	}
	putchar('\n');

	bool found = false;
	for (k = 0; k < prop->count_enums; ++k) {
	  if (prop->enums[connector->prop_values[j]].value == prop->enums[k].value) {
	    printf("\t\t\tCurrent enum value:\n"
		   "\t\t\t\t%s\n", prop->enums[k].name);
	    found = true;
	    break;
	  }
	}
	if (!found) {
	  puts("\t\t\tNo value currently set");
	}
	continue;
      }

      if (prop->flags & DRM_MODE_PROP_RANGE) {
	puts("\t\t\tPossible range:");
	const char *sep = "\t\t\t\t";
	for (k = 0; k < prop->count_values; ++k) {
	  printf("%s%"PRIu64, sep, prop->values[k]);
	  sep = "-";
	}
	putchar('\n');

	printf("\t\t\tCurrent range value:\n"
	       "\t\t\t\t%"PRIu64"\n", connector->prop_values[j]);

	continue;
      }

      if (prop->flags & DRM_MODE_PROP_BITMASK) {
	// Bitmask is like the enum above, except that its values are in range
	// [0,63]
	printf("\t\t\tAmount of values in bitmask: %d\n", prop->count_enums);

	puts("\t\t\tPossible bitmask values:");
	const char *sep = "\t\t\t\t";
	for (k = 0; k < prop->count_enums; ++k) {
	  printf("%s%s", sep, prop->enums[k].name);
	}
	putchar('\n');

	bool found = false;
	for (k = 0; k < prop->count_enums; ++k) {
	  if (prop->enums[connector->prop_values[j]].value == prop->enums[k].value) {
	    printf("\t\t\tCurrent bitmask value:\n"
		   "\t\t\t\t%s\n", prop->enums[k].name);
	    found = true;
	    break;
	  }
	}

	if (!found) {
	  puts("\t\t\tNo value currently set");
	}
	continue;
      }

      drmModeFreeProperty(prop);
    }
  }

  drmModeFreeResources(resources);
 err:
  close(cardFD);
  return ret;
}

const char *get_prop_type(uint32_t prop_flags)
{
  // First let's check the primary type of the property
  // Then we can check if it is immutable or not

  size_t tmp;
  if (prop_flags & DRM_MODE_PROP_RANGE)   tmp = 0;
  if (prop_flags & DRM_MODE_PROP_ENUM)    tmp = 1;
  if (prop_flags & DRM_MODE_PROP_BLOB)    tmp = 2;
  if (prop_flags & DRM_MODE_PROP_BITMASK) tmp = 3;

  if (prop_flags & DRM_MODE_PROP_IMMUTABLE) tmp += 4;

  // And now just return the proper string according to the type...
  return prop_type[tmp];
}

void parse_and_print_edid(const uint8_t *bytes)
{
  char PNPID[3];

  /**
   * To parse the PNP ID from the EDID
   * you must look at the 8th and 9th bytes
   * (0-indexed)
   *
   * In these two bytes, three letters are packed,
   * in a way where 'A' is 1 and 'Z' is 26.
   */

  puts("\t\t\tEDID information:");

  PNPID[0] = 'A' - 1 + ((bytes[8] >> 2) & 0x1F);
  PNPID[1] = 'A' - 1 + ((bytes[8] << 3) & 0x18) + ((bytes[9] >> 5) & 0x07);
  PNPID[2] = 'A' - 1 + (bytes[9] & 0x1F);

  printf("\t\t\t\tPNP ID:\n"
	 "\t\t\t\t\t%.3s\n", PNPID);

  printf("\t\t\t\tProduct code:\n"
	 "\t\t\t\t\t%"PRIu16"\n",
	 (((uint16_t)(bytes[10] << 8))+((uint16_t)(bytes[11]))));

  /**
   * Now we can check the interesting things,
   * such as the monitor name and serial number
   */
  char strBuf[13];
  for (const uint8_t *iter = bytes + 54; iter != bytes + 126; iter += 18) {
    if (*(iter + 3) != 0xFC) continue; // If not monitor name, go to next one

    for (size_t tmp = 5; tmp < 18; ++tmp) {
      if (*(iter + tmp) == '\n') {
	strBuf[tmp - 5] = '\0';
	break;
      }
      strBuf[tmp - 5] = *(iter + tmp);
    }
    printf("\t\t\t\tMonitor name:\n"
	   "\t\t\t\t\t%.13s\n", strBuf);
  }

  for (const uint8_t *iter = bytes + 54; iter != bytes + 126; iter += 18) {
    if (*(iter + 3) != 0xFF) continue; // If not monitor serial number, go to next one

    for (size_t tmp = 5; tmp < 18; ++tmp) {
      if (*(iter + tmp) == '\n') {
	strBuf[tmp - 5] = '\0';
	break;
      }
      strBuf[tmp - 5] = *(iter + tmp);
    }
    printf("\t\t\t\tMonitor serial:\n"
	   "\t\t\t\t\t%.13s\n", strBuf);
  }
}
